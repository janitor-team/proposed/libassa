// -*- c++ -*-
//------------------------------------------------------------------------------
//                               LogConn.cpp
//------------------------------------------------------------------------------
// $Id: LogConn.cpp,v 1.4 2006/07/20 02:30:55 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2003 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Date: Wed Apr 30 19:07:30 EDT 2003
//------------------------------------------------------------------------------

#include <unistd.h>
#include <iostream>
#include <algorithm>

#include "LogConn.h"
#include "LogMon.h"				// REACTOR declaration

int
LogConn::
open ()
{
	trace ("LogConn::open");

	ASSA::IPv4Socket& s = *this;

	REACTOR->registerIOHandler (this, s.getHandler (), ASSA::READ_EVENT);
	REACTOR->registerIOHandler (this, STDIN_FILENO, ASSA::READ_EVENT);

	DL((ASSA::APP,"Connected to assa-logd\n"));
	return 0;
}

void
LogConn::
close ()
{
	trace ("LogConn::close");

	DL((ASSA::APP, "Failed to connection to assa-logd!\n"));
	LOGMON->stop_service ();
}

int 
LogConn::
handle_close (int fd_)
{
	trace ("LogConn::handle_close");

	if (fd_ != STDIN_FILENO) { 
		DL((ASSA::APP,"Disconnected from assa-logd\n"));
	}
	return 0;
}

int
LogConn::
handle_read (int fd_)
{
	trace ("LogConn::handle_read");
	ASSA::IPv4Socket& s = *this;

	if (fd_ == STDIN_FILENO) {
		return process_user_request (s);
	}

    int ret = 0;
    char buf [256];
	if (s.getHandler () != fd_) { return -1; }

    if ((ret = s.read (buf, 256)) < 0) {
		DL((ASSA::ASSAERR,"Error reading from socket\n"));
		return (-1);
    }
	buf [ret] = '\0';
	std::cout << buf << std::flush;

	return BYTES_LEFT_IN_SOCKBUF(s);
}

int
LogConn::
process_user_request (ASSA::IPv4Socket& s_)
{
	trace("LogConn::process_user_request");

	std::string input;
	getline (std::cin, input);
	DL((ASSA::APP,"Requiest from user: \"%s\"\n", input.c_str ()));
	s_.write (input.c_str (), input.length ());
	s_.write (m_eor, 2);
	s_ << ASSA::flush;
	return 0;
}
