// -*- c++ -*-
//------------------------------------------------------------------------------
//                            MemDump.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//
// $Source: /cvsroot/libassa/libassa/assa/MemDump.cpp,v $
// $Revision: 1.6 $
// $Locker:  $
// $Author: vlg $
// $Date: 2006/07/20 02:30:54 $
//------------------------------------------------------------------------------

#include "assa/Logger.h"
#include "assa/MemDump.h"

using namespace ASSA;

const char MemDump::m_empty_str[] = "Null";

MemDump::
MemDump(const char* msg_, int len_) : m_dump (NULL)
{
	register int i;		// ptr into source buffer
	register int j;		// pair elements counter
	register int k;		// pairs counter [0;16[
	
	const char *p;		// ptr into source buffer
	char *hex;		// hex ptr into destination buffer
	char *ascii;		// ascii ptr into destination buffer
	
	long final_len;

	/*--- Precondition ---	*/
	if (len_ <= 0 || msg_ == (char*) NULL) {
		DL((ASSAERR,"No data to process.\n"));
		DL((ASSAERR,"Data length requested: %d <= 0!\n", len_));
		return;
	}
	j = k = 1;

	/*---
	  Each row holds 16 bytes of data. It requres 74 characters maximum.
	  Here's some examples:

0         1         2         3         4         5         6         7
0123456789012345678901234567890123456789012345678901234567890123456789012
-------------------------------------------------------------------------
3132 3037 3039 3039 3031 3130 3839 3033  1207090901108903
3038 3132 3030 3331 3030 0d0a 3839 3033  0812003100\r\n8903
0d0a 0d0a 0d0a 0d0a 0d0a 0d0a 0d0a 0d0a  \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n

      If all 16 bytes are control characters, the ASCII representation
	  will extend line to 72 characters plus cartrige return and line 
	  feed at the end of the line.

	  If len_ is not multiple of 16, we add one more row and another row
	  just to be on a safe side.
	  ---*/
	
	final_len = (int (len_/16) + 1 + (len_ % 16 ? 1:0)) * 74;
	
	m_dump = new char[final_len];
	memset (m_dump, ' ', final_len);
	
	p     = msg_;				// ptr to original image
	hex   = m_dump;				// current ptr to hex image
	ascii = m_dump + 41;		// current ptr to ascii image
	
	for (i = 0; i < len_; i++) 
	{
		sprintf(hex,"%01x%01x", p[i] >> 4 & 0x0f, p[i] & 0x0f); 
		hex+=2;

		if      (p[i] == '\n') { sprintf(ascii,"\\n"); ascii+=2; }
		else if (p[i] == '\t') { sprintf(ascii,"\\t"); ascii+=2; }
		else if (p[i] == '\v') { sprintf(ascii,"\\v"); ascii+=2; }
		else if (p[i] == '\b') { sprintf(ascii,"\\b"); ascii+=2; }
		else if (p[i] == '\r') { sprintf(ascii,"\\r"); ascii+=2; }
		else if (p[i] == '\f') { sprintf(ascii,"\\f"); ascii+=2; }
		else if (p[i] == '\a') { sprintf(ascii,"\\a"); ascii+=2; }
		else if (p[i] == '\0') { sprintf(ascii,"\\0"); ascii+=2; }
		else {
			sprintf (ascii++,"%c", ((p[i] < ' ' || p [i] > '~') ? '.' : p [i]));
		}
		
		if (!(j++ % 2)) {
			sprintf (hex++," ");
		}

		k %= 16;

		if (!(k++)) {
			*hex = ' ';
			sprintf (ascii++,"\n");
			hex = ascii;
			ascii +=  41;
		}
	}
	*hex = ' ';
	m_dump [final_len-1] = '\0';
}

void
MemDump::
dump_to_log (unsigned long mask_, const char* info_, const char* msg_, int len_)
{
    /* A very important shortcut (performance-wise)
	 * It saves on constructing unnecessary MemDump object when
	 * message logging for that particular group is disabled.
	 */

	if (LOGGER->group_enabled (static_cast<Group> (mask_)) && len_ > 0) 
	{		
		MemDump temp (msg_, len_);
		DL((mask_, "(%d bytes) %s\n", len_, info_));
		DL((mask_, "\n\n%s\n\n", temp.getMemDump ()));
	}
}

