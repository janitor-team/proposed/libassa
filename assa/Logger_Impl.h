// -*- c++ -*-
//------------------------------------------------------------------------------
//                              Logger_Impl.h
//------------------------------------------------------------------------------
// $Id: Logger_Impl.h,v 1.12 2012/05/21 03:20:39 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2001 Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef LOGGER_IMPL_H
#define LOGGER_IMPL_H

#include <errno.h>
#include <string>
#include <stdio.h>

#if defined(sun)
#include <sys/varargs.h>		// va_list
#endif

#if defined (__CYGWIN32__) || defined (__NetBSD__) || defined (WIN32) || defined (__GLIBC__)
# include <stdarg.h>
#endif

#if defined(WIN32)
#    include <winsock2.h>		/* select(3) */
#endif

/* Also defined in winsock.h, winsock2.h, gmon.h and in cygwin's sys/types
*/
#if !defined ( _BSDTYPES_DEFINED )

typedef unsigned char	u_char;
typedef unsigned short	u_short;
typedef unsigned int	u_int;
typedef unsigned long	u_long;

#define _BSDTYPES_DEFINED

#endif /* ! def _BSDTYPES_DEFINED  */

using std::string;
using std::ostream;

#include "assa/LogMask.h"

/** Sort out WIN32/mingw oddities
 */
#if defined (WIN32)

typedef SOCKET handler_t;
#define BAD_HANDLER INVALID_SOCKET

/** Map win32 error names
 */
#define EINPROGRESS   WSAEINPROGRESS   /* A blocking Winsock call is in
                                        * progress, or the service provider
                                        * is still process a callback function.
										*/
#define EWOULDBLOCK   WSAEWOULDBLOCK   /* The socket is marked as nonblocking
										* and the connection cannot be completed
										* immediately.
										*/
#define EISCONN       WSAEISCONN

#define ENOTSOCK      WSAENOTSOCK      /* The descriptor is not a socket.
										*/
#define ECONNREFUSED  WSAECONNREFUSED  /* The attempt to connect was
										* forcefully rejected.
										*/
#define ETIMEDOUT     WSAETIMEDOUT     /* An attempt to connect timed out
										* without establishing connection.
										*/
#else    /*--- POSIX ---*/

#define BAD_HANDLER -1
typedef int handler_t;

#endif	// ifdef WIN32


namespace ASSA {

class Reactor;

//---------------------------------------------------------------------------
// Utilities that don't fit anywhere else
//---------------------------------------------------------------------------

	/** Detect socket() error in a portable way.
	 *
	 *  @return true if socket is in valid range;
	 *          false otherwise.
	 */
	inline bool is_valid_handler (handler_t socket_)
	{
		return (socket_ != BAD_HANDLER);
	}

	/** Set socket descriptor to invalid value in a portable way.
	 *  socket_ is set to the value out of valid range.
	 */
	inline void disable_handler (handler_t& socket_)
	{
		socket_ = BAD_HANDLER;
	}

	/** Fetch error number in a portable way.
	 */
	inline int get_errno ()
	{
		int myerrno;
#if defined (WIN32)
		myerrno = WSAGetLastError ();
#else
		myerrno = errno;
#endif
		return myerrno;
	}

	/** Set error number in a portable way.
	 */
	inline void set_errno (int new_errno_)
	{
#if defined (WIN32)
        WSASetLastError (new_errno_);
#else
		errno = new_errno_;
#endif
	}

//---------------------------------------------------------------------------
// Class Logger_Impl
//---------------------------------------------------------------------------

class Logger_Impl {
public:
    /**
		Maximum length of the formatted message. The size is selected
		based on the maximum number of bytes transmitted through Socketbuf
		which is 1416. This is at most the bytes dumped with MemDump -
		(1416/16 + 2) * 74 = 6660. See MemDump.cpp comments for details.
	*/
	static const unsigned int LOGGER_MAXLINE = 6660;

public:
    Logger_Impl ();
    virtual ~Logger_Impl () { /* empty */ }

    void enable_group  (Group g_)  { m_groups |= g_;  }
    void disable_group (Group g_)  { m_groups &= ~g_; }

    void enable_groups  (u_long g_)  { m_groups |= g_;  }
    void disable_groups (u_long g_)  { m_groups &= ~g_; }

    void enable_all_groups  (void) { m_groups = ASSA::ALL;  }
    void disable_all_groups (void) { m_groups = 0;    }

    bool group_enabled (Group g_) const { return (m_groups & g_); }

    void enable_timestamp  (void)       { m_tmflg = true;  }
    void disable_timestamp (void)       { m_tmflg = false; }
    bool timestamp_enabled (void) const { return m_tmflg;  }
    void set_timezone      (int zone_)  { m_tz = zone_;    }

    void    set_indent_step (u_short step_) { m_indent_step = step_; }
    u_short get_indent_step (void) const    { return m_indent_step;  }

    /// Open StdErr Logger
    virtual int log_open (u_long groups_);

    /// Open File Logger
    virtual int log_open (const char* logfname_,
						  u_long groups_,
						  u_long maxsize_);

    /// Open connection with Log Server
    virtual int log_open (const char*  appname_,
						  const char* logfname_,
						  u_long        groups_,
						  u_long       maxsize_,
						  Reactor*     reactor_);

    virtual int  log_close  (void) = 0;
    virtual void log_resync (void) { /* empty */ }

    virtual int log_msg (Group g_,
						 size_t indent_level_,
						 const string& func_name_,
						 size_t expected_sz_,
						 const char* fmt_,
						 va_list) = 0;

    virtual int log_func (Group g_,
						  size_t indent_level_,
						  const string& func_name_,
						  marker_t type_) = 0;
protected:
    virtual u_short add_timestamp    (ostream& sink_);
    virtual u_short indent_func_name (ostream& sink_,
									  const string& funcname_,
									  size_t indent_level_,
									  marker_t type_);

	/** Format and put the message in the buffer. If expected size
		is smaller then LOGGER_MAXLINE, formatted message is written
		to the static buffer and release_ is set to false.
		Otherwise, this function allocates a buffer on the heap big
		enough to hold the message and set release_ to true. In this
		case caller is responsible for releasing the memory by calling
		delete [].

		@param expected_sz_ Expected size of the formatted message
		@param fmt_ printf()-like format string
		@param vap_ variable argument parameters list
		@param release_ [OUT] if true, caller is responsible for memory
		deallocation.
		@return Pointer to the formatted message buffer. If formatting
		failed, NULL is returned.
	*/
	char* format_msg (size_t expected_sz_,
					  const char*    fmt_,
					  va_list        vap_,
					  bool&      release_);

protected:
	/// Static buffer for formatted message
	static char m_msgbuf [LOGGER_MAXLINE];

    /// Indentation step
    u_short m_indent_step;

    /// Enabled groups
    u_long m_groups;

    /// Log file name
    string m_logfname;

    /// Timestamp on/off flag
    bool   m_tmflg;

    /// Timezone: 0-GMT, 1-Local
	int    m_tz;
};

inline
Logger_Impl::
Logger_Impl ()
    : m_indent_step (1),
	  m_groups (0),
	  m_tmflg (false),
	  m_tz (1)
{
    /* no-op */
}

inline int
Logger_Impl::
log_open (u_long /* groups_ */)
{
    errno = ENOSYS;
    return -1;
}

inline int
Logger_Impl::
log_open (const char*, /* logfname_ */
		  u_long,      /* groups_ */
		  u_long       /* maxsize_ */)
{
    errno = ENOSYS;
	return -1;
}

inline int
Logger_Impl::
log_open (const char*, /* appname_  */
		  const char*, /* logfname_ */
		  u_long,      /* groups_   */
		  u_long,	   /* maxsize_  */
		  Reactor*     /* reactor_  */)
{
    errno = ENOSYS;
	return -1;
}

} // end namespace ASSA

#endif /* LOGGER_IMPL_H */
