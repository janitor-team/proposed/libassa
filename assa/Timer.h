// -*- c++ -*-
//------------------------------------------------------------------------------
//                             Timer.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999,2005 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Create: 09/27/1999
//------------------------------------------------------------------------------
#ifndef TIMER_H
#define TIMER_H

#if !defined(WIN32)
#   include <sys/select.h>
#endif

#include <sys/time.h>

#include "EventHandler.h"
#include "assa/TimeVal.h"

namespace ASSA {

class TimerQueue;		// forward declaration

/** @file Timer.h 
 *
 * Timer class represents tuple that is stored in TimerQueue.
 */

class Timer 
{
public:
	/// Default constructor
	Timer ();

	/** Constructor used by the TimerQueue.
		@param eh_    EventHandler to call upon timeout
		@param tm_    Time of the day to expire the timer.
		@param delta_ Absolute timeout value.
		@param name_  Timer name
	*/
	Timer (const EventHandler* eh_, 
		   const TimeVal&      tm_,
		   const TimeVal&      delta_,
		   const std::string&  name_);

	/// Copy constructor
	Timer (const Timer& t_);

	/// Destructor: do-nothing
	~Timer ();

	/// Assignment operator
	Timer& operator= (const Timer& t_);

	/// Less-that by time
	bool operator< (const Timer& t_) const;

	/// Equal by time
	bool operator== (const Timer& t_) const;

	/// Get EventHandler pointer
	EventHandler* getHandler () const { return m_eh; }

	/// Get Expiration Time
	const TimeVal& getExpirationTime () const { return m_timer; }

	/// Get Delta time
	const TimeVal& getDeltaTime () const { return m_delta; }

	/// Reschedule expiration time with Delta value
	void rescheduleExpirationTime ();

	/// Dump contents to logfile
	void dump (void);

	/** Set Timer ID. ID allows Reactor and application-level
		code describe intelligently the kind of the Timer this is.
	*/
	void set_id (const std::string& id_) { m_id = id_; }

	/** Retrieve Timer ID.
	 */
	std::string get_id () const { return m_id; }

private:
	/// Pointer to EventHandler
	EventHandler* m_eh;

	/// When the timer should be triggered
	TimeVal m_timer;

	/// Absolute value used when Reactor needs to reschedule the timer
	TimeVal m_delta;

	/// Timer's ID 
	std::string m_id;
};
//------------------------------------------------------------------------------
// Timer class inlines
//------------------------------------------------------------------------------

inline	
Timer::
Timer () 
	: m_eh (NULL), m_id ("<unknown>")
{
	trace("Timer::Timer");
}

inline
Timer::
Timer (const EventHandler* eh_, const TimeVal& tm_,
	   const TimeVal& delta_, const std::string& name_)
	: m_eh ((EventHandler*) eh_), m_timer (tm_), m_delta (delta_), m_id (name_)
{
	trace("Timer::Timer(EH*, TV&)");
}

inline
Timer::
Timer (const Timer& t_)
	: m_eh (t_.m_eh), m_timer (t_.m_timer), 
	  m_delta (t_.m_delta), m_id (t_.m_id)
{
	trace("Timer::Timer(Timer&)");
}

inline
Timer::
~Timer ()
{
	trace("Timer::~Timer");
}

inline Timer& 
Timer::
operator=(const Timer& t_)
{
	m_eh    = t_.m_eh;
	m_timer = t_.m_timer;
	m_delta = t_.m_delta;
	m_id    = t_.m_id;

	return *this;
}

inline bool 
Timer::
operator<(const Timer& t_) const
{
	return m_timer < t_.m_timer;
}

inline bool 
Timer::
operator==(const Timer& t_) const
{
	return m_timer == t_.m_timer;
}

inline void 
Timer::
rescheduleExpirationTime ()
{
	TimeVal now (TimeVal::gettimeofday ());
	m_timer = now + m_delta;
}

inline void
Timer::
dump (void)
{
	DL((REACT,"Timer %s (EH=%s) expires at %s (delta=%s)\n",
		get_id ().c_str (),
		m_eh->get_id ().c_str (), 
		m_timer.fmtString ().c_str(),
		m_delta.fmt_mm_ss_mls ().c_str()));
}

/** 
 * TimerCompare class compares two Timers base on their expiration timestamp.
 */
struct TimerCompare 
{
	bool operator() (const Timer* t1_, const Timer* t2_) const 
		{
			return (*t1_ < *t2_);
		}
};

} // end namespace ASSA

#endif /* TIMER_H */  
