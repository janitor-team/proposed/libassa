// -*- c++ -*-
//------------------------------------------------------------------------------
//                             UNIXAddress.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 03/22/99
//------------------------------------------------------------------------------
#ifndef UNIX_ADDRESS_H
#define UNIX_ADDRESS_H

#if !defined(WIN32)

#include "assa/Address.h"

namespace ASSA {

/** @file UNIXAddress.h 
 *
 * UNIXAddress encapsulates UNIX domain socket address structure.
 */

class UNIXAddress : public Address {
public:
	/** Constructor.
	 * @param socket_name_ UNIX path name
	 */
	UNIXAddress (const char * socket_name_);

	/** Copy constructor.
	 * @param socket_address_ address to copy from
	 */
	UNIXAddress (SA* socket_address_);

	/// Destructor
	virtual ~UNIXAddress ();

	/// Retrieve address length
	const int getLength () const;

	/// Retrieve underlying address structure
	SA* getAddress () const;
	
private:
	/// UNIX socket address structure
	SA_UN m_address;
};

inline
UNIXAddress::
~UNIXAddress () { trace("UNIXAddress::~UNIXAddress"); }

inline const int 
UNIXAddress::
getLength () const { return sizeof (m_address); }

inline SA* 
UNIXAddress::
getAddress () const { return (SA*) &m_address; }

} // end namespace ASSA

#endif  /* !defined WIN32 */
   
#endif /* UNIX_ADDRESS_H */
