// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: XDRHack.h,v 1.2 2006/07/20 02:30:54 vlg Exp $
//------------------------------------------------------------------------------
//                               XDRHack.h
//------------------------------------------------------------------------------
//  Copyright (c) 2005 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 04/20/2005
//------------------------------------------------------------------------------
#ifndef XDRHACK_H
#define XDRHACK_H

/** @file XDRHack.h 

    XDRHack provides XDR definitions for systems that have them missing.
*/

/** 
 * xdr.h included with Cygwin's surpc package is not ANSI-compliant.
 * We define only what we need. Taken directly from rpc/xdr.h.
 *
 *   xdr.h, External Data Representation Serialization Routines.
 *   Copyright (C) 1984, Sun Microsystems, Inc.
 */
#if defined (__CYGWIN32__)
extern "C" {
#include <rpc/types.h>

enum xdr_op {
	XDR_ENCODE=0,
	XDR_DECODE=1,
	XDR_FREE=2
};

typedef struct __rpc_xdr {
    enum xdr_op x_op;       /* operation; fast additional param */
    const struct xdr_ops {
        /* get a long from underlying stream 
		 */
        bool_t  (*x_getlong)(struct __rpc_xdr *, long *);
        /* put a long to " 
		 */
        bool_t  (*x_putlong)(struct __rpc_xdr *, const long *);
        /* get some bytes from " 
		 */
        bool_t  (*x_getbytes)(struct __rpc_xdr *, char *, u_int);
        /* put some bytes to " 
		 */
        bool_t  (*x_putbytes)(struct __rpc_xdr *, const char *, u_int);
        /* returns bytes off from beginning 
		 */
        u_int   (*x_getpostn)(struct __rpc_xdr *);
        /* lets you reposition the stream 
		 */
        bool_t  (*x_setpostn)(struct __rpc_xdr *, u_int);
        /* buf quick ptr to buffered data 
		 */
        int32_t *(*x_inline)(struct __rpc_xdr *, u_int);
        /* free privates of this xdr_stream 
		 */
        void    (*x_destroy)(struct __rpc_xdr *);
        bool_t  (*x_control)(struct __rpc_xdr *, int, void *);
    } *x_ops;

	caddr_t 	x_public;	/* users' data */
	caddr_t		x_private;	/* pointer to private data */
	caddr_t 	x_base;		/* private used for position info */
	int		    x_handy;	/* extra private word */
} XDR;

#define	XDR_DESTROY(xdrs) \
	if ((xdrs)->x_ops->x_destroy) \
		(*(xdrs)->x_ops->x_destroy)(xdrs)

#define	xdr_destroy(xdrs) \
	if ((xdrs)->x_ops->x_destroy) \
		(*(xdrs)->x_ops->x_destroy)(xdrs)

	typedef	bool_t (*xdrproc_t)();

	extern bool_t xdr_opaque      (XDR *, caddr_t, u_int);
	extern bool_t xdr_string      (XDR *, char **, u_int);
	extern bool_t xdr_vector      (XDR *, char *,  u_int, u_int, xdrproc_t);
	extern void   xdrmem_create   (XDR *, char *,  u_int, enum xdr_op);
	extern void   xdrstdio_create (XDR *, FILE *,  enum xdr_op);
	extern bool_t xdr_int         (XDR *, int *);
	extern bool_t xdr_float       (XDR *, float *);
	extern bool_t xdr_double      (XDR *, double *);
	extern bool_t xdr_char        (XDR *, char *);
}

#else
#  include <rpc/rpc.h>
#endif

#endif /* XDRHACK_H */
