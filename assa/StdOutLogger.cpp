// -*- c++ -*-
//------------------------------------------------------------------------------
//                              StdOutLogger.cpp
//------------------------------------------------------------------------------
// $Id: StdOutLogger.cpp,v 1.4 2006/07/20 02:30:54 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2001 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//
//------------------------------------------------------------------------------
//  Created: 10/31/2001
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdarg.h>				// vsprintf(3)
#include <string>
#include <iomanip>

#include "assa/StdOutLogger.h"

using namespace ASSA;

int
StdOutLogger::
log_msg (Group          g_,
		 size_t         indent_level_, 
		 const string&  func_name_,
		 size_t         expected_sz_,
		 const char*    fmt_,
		 va_list        msg_list_)
{
	bool release = false;
	char* msgbuf_ptr = NULL;

    if (! group_enabled (g_)) {
		return 0;
    }

    add_timestamp (std::cerr);
    indent_func_name (std::cerr, func_name_, indent_level_, FUNC_MSG);

    msgbuf_ptr = format_msg (expected_sz_, fmt_, msg_list_, release);

	if (msgbuf_ptr == NULL) {
		return -1;				// failed to format
	}
	std::cout << msgbuf_ptr;

	if (release) {
		delete [] msgbuf_ptr;
	}
    return 0;
}

int
StdOutLogger::
log_func (Group         g_, 
		  size_t        indent_level_, 
		  const string& func_name_,
		  marker_t      type_) 
{
    if (! group_enabled (g_)) {
		return 0;
    }

    add_timestamp (std::cerr);
    indent_func_name (std::cout, func_name_, indent_level_, type_);

	if (type_ == FUNC_ENTRY) {
		std::cout << "---v---\n";
	}
	else if (type_ == FUNC_EXIT) {
		std::cout << "---^---\n";
	}
	
    return 0;
}

