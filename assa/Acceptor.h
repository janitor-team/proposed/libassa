// -*- c++ -*-
//------------------------------------------------------------------------
//                            Acceptor.h
//------------------------------------------------------------------------
//  Copyright (C) 1999  Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------
#ifndef ACCEPTOR_H
#define ACCEPTOR_H

#include "assa/Logger.h"
#include "assa/EventHandler.h"
#include "assa/Address.h"
#include "assa/Reactor.h"
#include "assa/ServiceHandler.h"

/** @file Acceptor.h

   Acceptor encapsulates generic strategy for accepting TPC/IP connection 
   requests.

   This abstract class implementes the generic strategy for passive
   initializing communication services.

   SERVICE_HANDLER is the type of service. It shall be a type derived
   from ServiceHandler interface class.
   
   PEER_ACCEPTOR is the type of concrete Socket class - particular
   transport mechanism used by the Acceptor to passively establish 
   the connection. It should be derived from Socket interface class.

   @see Reactor
   @see ServiceHandler
*/

namespace ASSA {

template<class SERVICE_HANDLER, class PEER_ACCEPTOR>
class Acceptor : public virtual EventHandler
{
public:
    /** Default constructor. 
		@param r_ Reactor to use
    */
    Acceptor (Reactor* r_);

    /** Do-nothing destructor. Underlying PEER_ACCEPTOR stream
		will be closed during its own destruction sequence.
    */
    virtual ~Acceptor ();
	
    /** Initialize listener endpoint and Acceptor with Reactor.
		Derive classes can change this strategy by overloading
		this method.

		@return 0 on success, -1 on error. An error can be if
		either PEER_ACCEPTOR's open() or bind() failed.
    */
    virtual int open (const Address& local_addr_);

    /** Close PEER_ACCEPTOR stream.
		@return 0 on success, -1 on error.
    */
    virtual int close (void);

    /** Callback invoked by Reactor when new connection requests 
		is detected. Default strategy is to accept ALL awaiting 
		incoming connections at once. Derived class can change 
		this strategy by overloading this method.

		@return 0 on success, -1 on error. Returning -1 will
		effectively instruct Reactor to remove this Handler from
		the Reactor.
    */
    int handle_read (int fd);

    /** Callback invoked by Reactor if PEER_ACCEPTOR stream 
		went bad, or Reactor has been commanded to stop event processing. 
		This method should always return -1, if stream cannot be repared.
		Derived class can change this strategy by overloading 
		this method. If called object is other then Reactor, an
		explicit call to Reactor::removeHandler (this->id()) is required.
		By default behavior, Acceptor will destroy itself.
	
		@return -1 always
    */
    virtual int handle_close (int fd);

protected:
    /** 
		Defines creation strategy for ServiceHandler.
		@return pointer to SERVICE_HANDLER
    */
    virtual SERVICE_HANDLER* makeServiceHandler (PEER_ACCEPTOR* sock_);

    /** 
		Default strategy is to accept new connection. Derived
		class can change this  strategy by overloading this method.
	
		@param new_socket_ On return will have a pointer to 
		newly created PEER_STREAM, or =0 if failed
		@return 0 on success, -1 on error.
    */
    virtual int acceptServiceHandler (PEER_ACCEPTOR*& new_socket_);

    /** Defines the concurrency strategy. Default is to create
		SERVICE_HANDLERin current process(thread), call its open()
		methid and let Reactor handle its I/O events.
		Derived class changes this strategy by overloading this class.
		
		@param new_socket_ [in] PEER_STREAM pointer to activate
		@return 0 on success, -1 on error.
    */
    virtual int activateServiceHandler (PEER_ACCEPTOR* new_socket_);

protected:
    /** Underlying communication stream.
     */
    PEER_ACCEPTOR m_listenSocket;

private:

    /** Reactor to use.
     */
    Reactor* m_reactor;
};

// Convenience definitions

#define SH SERVICE_HANDLER
#define PA PEER_ACCEPTOR

//------------------------------------------------------------------------------
// Template member functions definitions
//------------------------------------------------------------------------------

template<class SH, class PA>
inline
Acceptor<SH, PA>::
Acceptor (Reactor* r_) 
    : m_reactor (r_)
{
    trace("Acceptor::Acceptor");
}

template<class SH, class PA>
inline
Acceptor<SH, PA>::
~Acceptor () 
{
    trace("Acceptor::~Acceptor");
}

template<class SH, class PA> 
inline int 
Acceptor<SH, PA>::
close (void) 
{
    trace("Acceptor::close");
    m_listenSocket.close ();
    return 0;
}

template<class SH, class PA> 
inline int 
Acceptor<SH, PA>::
handle_close (int /* fd */) 
{
    trace("Acceptor::handle_close");
	
    // Reactor::get_instance ()->removeHandler (this->id());
	
    // NOT IMPLEMENTED: This spot requires validation
    // whether Acceptor is created on the heap or in
    // automatic memory.
	DL ((REACT,"Deleted acceptor \"%s\"\n", get_id ().c_str ()));
    delete this;
    return -1;
}

template<class SH, class PA> 
inline SERVICE_HANDLER* 
Acceptor<SH, PA>::
makeServiceHandler (PEER_ACCEPTOR* sock_) 
{
    trace("Acceptor<>::makeServiceHandler");
	
    return new SERVICE_HANDLER (sock_);
}

template<class SH, class PA> 
inline int 
Acceptor<SH, PA>::
acceptServiceHandler (PEER_ACCEPTOR*& new_socket_) 
{
    trace("Acceptor::acceptServiceHandler");
	
    new_socket_ = m_listenSocket.accept ();
    return new_socket_ ? 0 : -1;
}

template<class SH, class PA> int 
Acceptor<SH, PA>::
activateServiceHandler (PA* new_socket_) 
{
    trace("Acceptor::activateServiceHandler");

    if (!new_socket_) {
		return -1;
	}
    SH* sh = makeServiceHandler (new_socket_);
    if (sh->open () < 0) {
		sh->close ();
	}
    return 0;
}

template<class SH, class PA> int
Acceptor<SH, PA>::
open (const Address& local_addr_)
{
    trace("Acceptor::open");
	
    if ( !m_listenSocket.open (local_addr_.getAddress ()->sa_family) ) {
		return -1;
	}

    if ( !m_listenSocket.bind (local_addr_) ) {
		return -1;
	}

    m_reactor->registerIOHandler (
		this, m_listenSocket.getHandler (), READ_EVENT);
	
    DL((TRACE,"Opened acceptor for fd=%d\n", 
		m_listenSocket.getHandler ()));

    return 0;
}

//------------------------------------------------------------------------------
// Accept all connections waiting in listen queue at once. This avoids going 
// through Reactor's event loop for each new connection.
//------------------------------------------------------------------------------

template <class SH, class PA> int
Acceptor<SH, PA>::
handle_read (int fd_)
{
    trace("Acceptor<>::handle_read");

    FdSet mask;
    timeval poll = {0, 0};
    PA* new_socket = 0;

    int fd = m_listenSocket.getHandler ();

    if (fd != fd_) {
		return -1;
	}

    do {
		if ( acceptServiceHandler (new_socket) == -1 ) {
			return -1;
		}
		if ( !activateServiceHandler (new_socket) == -1 ) {
			return -1;
		}
		mask.reset ();
		mask.setFd (fd);
    }
    while ((::select (fd+1, &mask, NULL, NULL, &poll) == 1));

    return 0;
}

} // end namespace ASSA

#endif /* ACCEPTOR_H */  
