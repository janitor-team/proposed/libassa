// -*- c++ -*-
//------------------------------------------------------------------------------
//                               RemoteLogger.h
//------------------------------------------------------------------------------
// $Id: RemoteLogger.h,v 1.1 2003/07/25 02:51:47 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2003 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Created: 
//------------------------------------------------------------------------------
#ifndef LOG_SERVER_H
#define LOG_SERVER_H

#include "assa/ServiceHandler.h"
#include "assa/IPv4Socket.h"
#include "assa/Logger_Impl.h"

/*******************************************************************************
 Class 
*******************************************************************************/

namespace ASSA {

class Reactor;

/** @file RemoteLogger.h

    A proxy connection class with logging server, assa-logd.
	Application has no reconnection logic. Connection to the server
	is one-shot deal that happens when this class is instantated;
 */

class RemoteLogger : 
		public Logger_Impl, 
		public ASSA::ServiceHandler<ASSA::IPv4Socket>
{
public:
	RemoteLogger ();

	/** Called by Connector upon establishing connection
	 */
	virtual int open ();

	/** Called by Reactor when we close connection with log_close()
	 */
	virtual int handle_close (int fd_);

	/** Calling results into sending SIGN-ON message
	 */
    virtual int log_open (const char*  appname_, 
						  const char* logfname_, 
						  u_long        groups_, 
						  u_long       maxsize_,
						  Reactor*     reactor_);

	/** Close connection to the assa-logd 
	 */
    virtual int log_close (void);

	/// Flush output buffer.
    virtual void log_resync (void);

    virtual int log_msg (Group g_, size_t indent_level_, 
						 const string& func_name_, 
						 size_t expected_sz_,
						 const char* fmt_, va_list);

    virtual int log_func (Group g_, size_t indent_level_, 
						  const string& func_name_, 
						  marker_t type_);

private:
	RemoteLogger (const RemoteLogger&);
	RemoteLogger& operator= (const RemoteLogger&);

private:
	enum state_t { opened, closed };
	enum msg_t { SIGN_ON = 0, SIGN_OFF, LOG_MSG };

	state_t    m_state;
	Reactor* m_reactor;

    /// If true, recursive call is in progress
	bool m_recursive_call;		
};


} // @end namespace ASSA

#endif /* LOG_SERVER_H */

