// -*- c++ -*-
//------------------------------------------------------------------------------
//                               MaskSet.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef MASK_SET_H
#define MASK_SET_H

#include "assa/FdSet.h"

namespace ASSA {

/** @file MaskSet.h

     Bundles file descriptor mask sets to be used with select().
*/

class MaskSet
{
public:
	/// Read fds set
	FdSet m_rset;

	/// Write fds set
	FdSet m_wset;

	/// Exception fds set
	FdSet m_eset;

public:
	/// Clear all bits in all sets.
	void reset ();

	/// Resync internals after select() call.
	void sync ();

	/// Return maximum value of the file descriptor in the Set
	int  max_fd ();

	/// Write current state of MaskSet object to log file.
	void dump ();
};

inline void
MaskSet::
sync ()
{
	m_rset.sync ();
	m_wset.sync ();
	m_eset.sync ();
}


inline void
MaskSet::
reset ()
{
	m_rset.reset ();
	m_wset.reset ();
	m_eset.reset ();
}

inline int
MaskSet::
max_fd ()
{
	return (std::max (m_rset.maxInSet (),
					  std::max (m_wset.maxInSet (),
								m_eset.maxInSet ())));
}

inline void
MaskSet::
dump ()
{
	DL((REACTTRACE,"+---------------------------\n"));
	DL((REACTTRACE,"| RD FDs set %s\n", m_rset.dump_c_str ().c_str ()));
	DL((REACTTRACE,"| WR FDs set %s\n", m_wset.dump_c_str ().c_str ()));
	DL((REACTTRACE,"| EX FDs set %s\n", m_eset.dump_c_str ().c_str ()));
	DL((REACTTRACE,"+---------------------------\n"));
}

} // end namespace ASSA

#endif /* MASK_SET_H */
