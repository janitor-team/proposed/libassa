// -*- c++ -*-
//------------------------------------------------------------------------------
//                             Regexp.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2003  Vladislav Grinchenko <vlg@users.sourceforge.net>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <assa/Regexp.h>
using namespace ASSA;

Regexp::
Regexp (const std::string& pattern_) 
    :
    m_pattern (NULL),
    m_error_msg (new char [256]),
    m_compiled_pattern (new regex_t)
{
    trace_with_mask("Regexp::Regexp", REGEXP);

	m_pattern = new char [pattern_.size () + 1];
    ::strncpy (m_pattern, pattern_.c_str (), pattern_.size ());
    m_pattern [pattern_.size ()] = '\0';

    int ret = ::regcomp (m_compiled_pattern, m_pattern, REG_EXTENDED);

    if (ret != 0) {
		::regerror (ret, m_compiled_pattern, m_error_msg, 256);
		DL((REGEXP,"regcomp(\"%s\") = %d\n", m_pattern, ret));
		DL((REGEXP,"error: \"%s\"\n", m_error_msg));

		delete [] m_pattern;
		m_pattern = NULL;
    }
}

Regexp::
~Regexp ()
{
    trace_with_mask("Regexp::~Regexp", REGEXP);

    if (m_pattern) {
		delete [] m_pattern;
    }
    if (m_error_msg) {
		delete [] m_error_msg;
    }
    ::regfree (m_compiled_pattern);
	delete (m_compiled_pattern);
}

int
Regexp::
match (const char* text_)
{
    trace_with_mask("Regexp::match", REGEXP);

    if (text_ == NULL || m_pattern == NULL) {
		return -1;
    }

    /**
       regexec(3) returns zero for a successful match or
       REG_NOMATCH for failure.
    */
    int ret = ::regexec (m_compiled_pattern, text_, 0, NULL, 0);

    if (ret != 0) {
		::regerror (ret, m_compiled_pattern, m_error_msg, 256);
		DL((REGEXP,"regexec(\"%s\") = %d\n", text_, ret));
		DL((REGEXP,"pattern: \"%s\"\n", m_pattern));
		DL((REGEXP,"error: \"%s\"\n", m_error_msg));
    }

    return (ret == 0 ? 0 : -1);
}

