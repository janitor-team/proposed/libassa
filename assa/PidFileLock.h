// -*- c++ -*-
//------------------------------------------------------------------------------
//                           PidFileLock.h
//------------------------------------------------------------------------------
// $Id: PidFileLock.h,v 1.7 2006/07/20 02:30:54 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002,2005  Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef FILE_LOCK_H
#define FILE_LOCK_H

//System Includes
#include <sys/types.h>

#include "assa/Assure.h"

//STL
#include <string>
using std::string;

#if defined(WIN32)
struct flock {
       off_t l_start;
       off_t l_len;
       pid_t l_pid;
       short l_type;
       short l_whence;
};
#endif

namespace ASSA {

/** @file PidFileLock.h

A utility class for creating and managing process PID lock file.
*/

class PidFileLock   : public flock
{
public:
	/// Constructor. 
	PidFileLock ();

	/** Destructor. If process is holds the lock on the file, file
	    is removed from the file system.
	*/
	~PidFileLock ();

	/** Lock the file. 
	    @return true on success, false on error
	*/
	bool lock (const string& filename_);

	/** Return last <TT>errno</TT> value. 
	    @return 0 for success, <TT>errno</TT> on error
	*/
	int get_error () const;
	
	/** In case of error, return a verbal description of the 
	    last error.
	*/
	const char* get_error_msg () const;

	/** Write the state of the lock to debug file. <TT>m_fd = -1</TT>
	    indicates that lock file is not opened.
	*/
	void dump ();

private:
	/** Open pid file in a cross-platform way
	 */
	pid_t open_pid_file (const std::string& fname_);

	/** Lock the entire file.
	    @return -1 on error and if file is locked by some other
	    process, errno is set to EAGAIN or EACCES; 0 on success.
	*/
	int lock_region ();

	/** Lock the entire file (only under Cygwin).
	    @return -1 on error and if file is locked by some other
	    process, errno is set to EAGAIN or EACCES; 0 on success.
	*/
	int lock_region_exclusive ();

	/** Unlock the entire file.
	    @return -1 on error; 0 on success.
	*/
	int unlock_region ();

	/** Retrieve lock status

	    @return -1 on error if failed, 0 on success.
	*/
	int get_lock_status ();


	/** Write our process pid to the lock file.
	    @return -1 on error; 0 on success.
	*/
	int write_pid ();

	/** Test if file is unlocked.
	    @return 0 if file is unlocked or the pid of the process that holds 
		the lock otherwise.
	*/
	pid_t test_region ();

	/** Log an error message to the log file and set internal  error to errno.
	*/
	void log_error   (const char* msg_);

private:
	/// Lock file name
	string m_filename;

	/// Lock file descriptor
	int    m_fd;

	/// Last system call error
	int    m_error;

	/// Error explanation
	string m_error_msg;
};

inline int 
PidFileLock::
get_error () const 
{ 
	return m_error; 
}

inline const char* 
PidFileLock::
get_error_msg () const 
{ 
	return m_error_msg.c_str (); 
}

} // end namespace ASSA

#endif /* FILE_LOCK_H */  
