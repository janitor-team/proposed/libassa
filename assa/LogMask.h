// -*- c++ -*-
//------------------------------------------------------------------------------
//                              LogMask.h
//------------------------------------------------------------------------------
// $Id: LogMask.h,v 1.6 2006/07/20 02:30:54 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2001 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef LOG_MASK_H
#define LOG_MASK_H

/** @file LogMask.h
    Bit mask used by Logger.
*/

/** @enum Group
    Bit mask used to mask out log messages.
*/
namespace ASSA {
enum Group {
	TRACE        = 0x00000001, /**< Function call trace                      */
	APP          = 0x00000002, /**< Application-level messages               */
	USR1         = 0x00000004, /**< Additional application-level messages    */
	USR2         = 0x00000008, /**< Additional application-level messages    */
	USR3         = 0x00000010, /**< Additional application-level messages    */
	/*-----------------------------------------------------------------------*/
	ALL_APPS     = 0x0000001F, /**< All application-level messages           */
	/*-----------------------------------------------------------------------*/
	ASSAERR      = 0x00000020, /**< ASSA and system errors                   */
	PIDFLOCK     = 0x00000040, /**< Class PidFileLock messages               */
	CMDLINEOPTS  = 0x00000080, /**< Class CmdLineOpts messages               */
	SEM          = 0x00000100, /**< Class Semaphore messages                 */
	SIGHAND      = 0x00000200, /**< Class SigHandler(s) messages             */
	REACT        = 0x00000400, /**< Class Reactor/PrioriyQueue messages      */
	REACTTRACE   = 0x00000800, /**< Extended Reactor/PrioriyQueue messages   */
	SOCK         = 0x00001000, /**< Class Socket & friends messages          */
	SOCKTRACE    = 0x00002000, /**< Extended Socket & friends messages       */
	XDRBUF       = 0x00004000, /**< Class xdrIOBuffer messages               */
	XDRBUFTRACE  = 0x00008000, /**< Extended xdrIOBuffer messages            */
	STRMBUF      = 0x00010000, /**< Class Streambuf & friends messages       */
	STRMBUFTRACE = 0x00020000, /**< Extended Streambuf & friends messages    */
	FORK         = 0x00040000, /**< Class Fork messages                      */
	SIGACT       = 0x00080000, /**< Class SigACtion messages                 */
	PIPE         = 0x00100000, /**< Class Pipe messages                      */
	CHARINBUF    = 0x00200000, /**< Class CharInBuffer messages              */
	ADDRESS      = 0x00400000, /**< Class Address & friends messages         */
	INIFILE      = 0x00800000, /**< Class IniFile messages                   */
	REGEXP       = 0x01000000, /**< Class RegExp messages                    */
	RES5         = 0x02000000, /**< Reserved for future use                  */
	RES6         = 0x04000000, /**< Reserved for future use                  */
	RES7         = 0x08000000, /**< Reserved for future use                  */
	RES8         = 0x10000000, /**< Reserved for future use                  */
	RES9         = 0x20000000, /**< Reserved for future use                  */
	RES10        = 0x40000000, /**< Reserved for future use                  */
	/*-----------------------------------------------------------------------*/
	ALL_LIB      = 0x7FFFFFE0, /**< All library messages                     */
	ALL          = 0x7FFFFFFF, /**< All messages: library + application      */
	NONE         = 0x00000000  /**< Total silence                            */
};


enum marker_t { 
	FUNC_MSG,
	FUNC_ENTRY, 
    FUNC_EXIT 
};

} /* end namespace ASSA */

#endif /* LOG_MASK_H */
