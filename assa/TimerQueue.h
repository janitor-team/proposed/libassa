// -*- c++ -*-
//------------------------------------------------------------------------------
//                          TimerQueue.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999,2005 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 07/28/1999
//------------------------------------------------------------------------------
#ifndef TIMER_QUEUE_H
#define TIMER_QUEUE_H

#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include "assa/Logger.h"
#include "assa/EventHandler.h"
#include "assa/Timer.h"
#include "assa/PriorityQueue.h"

namespace ASSA {

typedef unsigned long TimerId;

/** @file TimerQueue.h 
 *
 * TimerQueue class represents the queue of Timers that application can
 * install for Reactor to dispatch.
 */
class TimerQueue
{
public:
	/// Constructor. 
	TimerQueue ();

	/// Destructor.
	~TimerQueue ();

	/** Is queue empty?
	   @return true if queue empty; false if not.
	*/
	bool isEmpty ();

	/** Add timer (EventHandler object) to the queue to be dispatch 
	    at the time specified.

	    @param eh_     Pointer to Event Handler that will be called
	                   when timer expires.
	    @param tv_     Absolute expiration time.
		@param delta_  Relative timeout value.
		@param name_   Name of the timer (for easy identification).

	    @return TimerId that uniquely identifies the timer. 
	            It can be used to cancel timer.
	*/
	TimerId insert (EventHandler* eh_, 
					const TimeVal& tv_, 
					const TimeVal& delta_,
					const std::string& name_);

	/** Cancel all timers for the EventHandler eh_.
	    @param eh_ Pointer to Event Handler.
	    @return Number timers removed.
	 */
	int remove (EventHandler* eh_);

	/** Cancel timer.
	    @param tid_ Timer id.
	    @return true if timer was found in the queue; false otherwise.
	 */
	bool remove (TimerId tid_);

	/** Traverse the queue, triggering all timers that are past 
	    argument timeval. Timer(s) are then removed from the queue.

	    @param tv_ Expiration time
	    @return Number of callbacks dispatched
	*/
	int expire (const TimeVal& tv_);

	/// Return expiration time of the top element in the queue.
	TimeVal& top (void);

	/// Dump Queue information to the log file
	void dump (void);

private:
	/// Timer queue itself.
	PriorityQueue <Timer*, TimerCompare> m_queue;
};

//------------------------------------------------------------------------------
// Inline functions
//------------------------------------------------------------------------------

inline
TimerQueue::
TimerQueue () 
{
	trace("TimerQueue::TimerQueue");
}

inline bool
TimerQueue::
isEmpty ()
{ 
	return m_queue.size () == 0;
}

inline TimeVal&
TimerQueue::
top (void)
{
	return (TimeVal&) m_queue.top ()->getExpirationTime ();
}

} // end namespace ASSA

#endif /* TIMER_QUEUE_H */  
