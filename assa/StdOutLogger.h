// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: StdOutLogger.h,v 1.3 2003/07/25 02:51:47 vlg Exp $
//------------------------------------------------------------------------------
//                            StdOutLogger.h
//------------------------------------------------------------------------------
//  Copyright (c) 2001 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 10/31/2001
//------------------------------------------------------------------------------
#ifndef STDOUT_LOGGER_H
#define STDOUT_LOGGER_H

#include <fstream>
#include <string>
#include <iostream>

#include "assa/Logger_Impl.h"

namespace ASSA {

/** @file StdOutLogger.h 
 *
 * StdOutLogger implements Logger as standard output.
 */

class StdOutLogger : public Logger_Impl
{
public:
	StdOutLogger () { /*empty*/ }

	virtual int log_open (u_long  groups_);
	virtual int log_close (void);

	virtual int log_msg (Group g_, 
						 size_t indent_level_,
						 const string& func_name_,
						 size_t expected_sz_,
						 const char* fmt_, va_list);

	virtual int log_func (Group g_, 
						  size_t indent_level_,
						  const string& func_name_,
						  marker_t type_);
	virtual void log_resync ();

private:
	StdOutLogger (const StdOutLogger&);            /// no cloning
	StdOutLogger& operator= (const StdOutLogger&); /// no cloning
};

/*******************************************************************************
 Inline member functions
*******************************************************************************/
inline int
StdOutLogger::
log_open (u_long groups_)
{
    m_groups   = groups_;
    return 0;
}

inline int
StdOutLogger::
log_close (void)
{
    return 0;
}

inline void
StdOutLogger::
log_resync ()
{
	std::cout << std::flush;
}


} // end namespace ASSA

#endif /* STDERR_LOGGER_H */
