// -*- c++ -*-
//------------------------------------------------------------------------------
//                              assa/Logger_Impl.cpp
//------------------------------------------------------------------------------
// $Id: Logger_Impl.cpp,v 1.7 2012/05/21 03:20:39 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <string.h>				// strerror(3)

#include "assa/TimeVal.h"
#include "assa/Logger_Impl.h"

#if defined (WIN32)
#  include <windows.h>			// for vsnprintf() bug
#else
#  include <stdio.h>
#endif

using namespace ASSA;

char Logger_Impl::m_msgbuf [LOGGER_MAXLINE];

u_short
Logger_Impl::
add_timestamp (ostream& sink_)
{
    /*--- 'DD/MM/CC HH:MM:SS.MMMM ' - 23 chars ---*/
    u_short bytecount = 0;

    if (timestamp_enabled ()) {
		TimeVal tv = TimeVal::gettimeofday ();
		tv.tz (m_tz);
		sink_ << tv.fmtString ("%m/%d/%Y %H:%M:%S") << '.';
		char oldfill = sink_.fill('0');
		sink_ << std::setw (3) << (tv.msec () % 1000000)/1000 << ' ';
		sink_.fill (oldfill);
		bytecount = 23;
    }
    return bytecount;
}

u_short
Logger_Impl::
indent_func_name (ostream&      sink_,
				  const string& func_name_,
				  size_t        indent_level_,
				  marker_t      type_)
{
    u_short bytecount = 0;

    if (func_name_.size ()) {
		u_int i = 1;
		while (i < indent_level_) {
			sink_ << '|';
			for (u_short j = 0; j < m_indent_step-1; j++) {
				sink_ << ' ';
			}
			i++;
		}
		if (type_ == FUNC_ENTRY) {
			sink_ << '/' << func_name_ << "  ";
		}
		else if (type_ == FUNC_EXIT) {
			sink_ << '\\' << func_name_ << "  ";
		}
		else if (type_ == FUNC_MSG) {
			sink_ << '[' << func_name_ << "] ";
		}
		bytecount += indent_level_ * m_indent_step + func_name_.size () + 3;
    }
    return bytecount;
}

char*
Logger_Impl::
format_msg (size_t      expected_sz_,
			const char* fmt_,
			va_list     vap_,
			bool&       release_) // tell the caller it needs to release memory
{
	char* msg = m_msgbuf;		// Use internal buffer
	int ret = 0;

	release_ = false;
	expected_sz_++;				// Expected size includes '\0'

	if (expected_sz_ >= LOGGER_MAXLINE) { // Allocate temporary buffer
		msg = new char [expected_sz_];
		release_ = true;
	}

	ret = ::vsnprintf (msg, expected_sz_, fmt_, vap_);
#if NEVER
	if (ret < 0) {
		std::cout << "Logger_Impl: format_mg(expected_sz=" << expected_sz_
				  << ")=-1 failed! errno=" << errno << " ("
				  << strerror(errno) << "\n" << std::flush;
	}
#endif

	return (ret < 0 ? NULL : msg);
}
