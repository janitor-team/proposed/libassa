// -*- c++ -*-
//------------------------------------------------------------------------------
//                            IdSet.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include "assa/Logger.h"
#include "assa/IdSet.h"

using namespace ASSA;

int
IdSet::
newid()
{
    register int i;
    register int current;

    trace("IdSet::newid");

    current = m_next_available_id++;

    if (m_next_available_id < FD_SETSIZE) 
    {
	// mark current id as being in use
	FD_SET(current, &m_id_set_map);

	// search starting from current position to the end 
	// assuming that m_next_available_id is maintained
	// to be the lowest available at all times.

	for (i=current+1; i<FD_SETSIZE; i++) 
	{
	    if (!FD_ISSET(i, &m_id_set_map)) 
	    {
		m_next_available_id = i;
		return current;
	    }
	}
	// if I am here, I am out of ids
	m_next_available_id = FD_SETSIZE;
    }
    return -1;
}

int
IdSet::
recycle(int id_)
{
    trace("IdSet::recycle");

    if ( 0 <= id_ && id_ < FD_SETSIZE ) {
		FD_CLR(id_, &m_id_set_map);	// mark id as free
		
		// if id is smaller then current, adjust current
		if (id_ < m_next_available_id) {
			m_next_available_id = id_;
		}
		return 0;
    }
    return -1;
}
