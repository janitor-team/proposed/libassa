// -*- c++ -*-
//------------------------------------------------------------------------------
//                                IdSet.h
//------------------------------------------------------------------------------
//  Copyright (c) 1997 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef ID_SET_H
#define ID_SET_H

#include <string.h>

#include <sys/time.h>		/* select(3) */

#if defined(Linux)		/* select(3) */
#    include <sys/types.h>
#    include <unistd.h>
#endif

#if defined(WIN32)
#    include <winsock2.h>
#endif

namespace ASSA {

/** @file IdSet.h 

    Class IdSet implements a set of reusable unique IDs, up to 1024.

	IdSet class is my shot at reusable ids, similar to file descriptors 
	provided by OS. ID numbers are recycable, up to 1024.
*/

class IdSet
{
public:
	/// Default constructor creates IdSet object with ID equals to 0.
	IdSet ();

	/** Return current id. Mark it as being in use and set
	    new current id value to the next lowest available.
	*/
	int newid ();

	/** Recycle id_. Mark it as available and adjust current
	    id if necessary.
	*/
	int recycle (int id_);

	/** Get current id. This function just returns current id without
	    changing anything.
	*/
	int currid () const;

private:
	/** Current id 
	 */
	int m_next_available_id;

	/** Map of all ids.
	 */
	fd_set m_id_set_map;
};

inline
IdSet::
IdSet()
	: m_next_available_id (0)
{
	::memset (&m_id_set_map, 0, sizeof (m_id_set_map));
}
 
inline int
IdSet::
currid() const
{
	return m_next_available_id;
}

} // end namespace ASSA

#endif /* ID_SET_H */
