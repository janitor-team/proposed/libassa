// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: Semaphore.cpp,v 1.5 2012/05/20 04:12:18 vlg Exp $
//------------------------------------------------------------------------------
//                              Semaphore.C
//------------------------------------------------------------------------------
//  Copyright (c) 2000 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 07/10/2000
//------------------------------------------------------------------------------

#if !defined(WIN32)

#include <sstream>
#include <string>
#include <iomanip>

#include "assa/Semaphore.h"

using namespace ASSA;

/*--- Static definitions and constants ---*/

const int ASSA::Semaphore::BIGCOUNT = 10000;

sembuf Semaphore::m_op_lock [2] =
{
	{2, 0, 0},					// Wait for [2] lock to equal 0,
	{2, 1, SEM_UNDO}			// then increment [2] to 1 - this locks it.
								// UNDO to release the lock if processes
								// exits before explicitly unlocking.
};

sembuf Semaphore::m_op_endcreate [2] =
{
	{1, -1, SEM_UNDO},			// Decrement [1] (proc counter) with undo on
								// exit,
	{2, -1, SEM_UNDO}			// then decrement [2] (lock) back to 0.
};

sembuf Semaphore::m_op_open [2] =
{
	{1, -1, SEM_UNDO},			// Decrement [1] (proc counter) with undo on
								// exit.
};

sembuf Semaphore::m_op_close [3] =
{
	{2, 0, 0},					// Wait for [2] lock to equal 0,
	{2, 1, SEM_UNDO},			// then increment [2] to 1 - this locks it,
	{1, 1, SEM_UNDO}			// then increment [1] (proc counter)
};

sembuf Semaphore::m_op_unlock [1] =
{
	{2, -1, SEM_UNDO}			// Decrement [2] (lock) back to 0
};

sembuf Semaphore::m_op_op [1] =
{
	{0, 99, SEM_UNDO}			// Decrement or increment [0] with undo on
								// exit. The 99 is set to the actual amount
								// to add or substract (positive or negative)
};

//------------------------------------------------------------------------------
int
Semaphore::
create (key_t key_, int initval_)
{
	trace_with_mask("Semaphore::create", SEM);

	register int semval;

	union semnum {
		int              val;
		struct semid_ds* buf;
		ushort*          array;
	} semctrl_arg;

	if (IPC_PRIVATE == key_) {
		EL((ASSAERR,"Not intended for private semaphores\n"));
		return (-1);
	}
	else if (key_ == (key_t) -1) {
		EL((ASSAERR,"Probably an ftok() error by caller\n"));
		return (-1);
	}

	m_key = key_;
	bool done = false;

	while (!done) {
		if ( (m_id = semget (m_key, 3, 0666 | IPC_CREAT)) < 0) {
			EL((ASSAERR,"Permission problem or kernel tables full\n"));
			return (-1);
		}
		/*
		  When the semaphore is created, we know that the value of
		  all 3 set members is 0.

		  Get a lock on the semaphore by waiting for [2] to equal 0,
		  then increment it.

		  There is a race condition here. There is a possibility
		  that between the semget(2) and semop(2) below, another
		  process can cal Semaphore:::close () member function
		  which can remove a semaphore if that process is the last
		  one using it.

		  Therefore, we handle the error condition of an invalid
		  semaphore ID specially below, and if it does happen, we
		  just go back and create it again.
		*/

		if (semop (m_id, &m_op_lock[0], 2) < 0) {
			if (errno == EINVAL) {
				continue;
			}
			EL((ASSAERR,"Can't lock semaphore\n"));
			Assure_exit (false);
		}
		done = true;
	} // while (!done)

	/*
	  Get the value of the process counter. If it equals 0,
	  then no one has initialized the semaphore yet.
	*/
	if ((semval = semctl (m_id, 1, GETVAL, 0)) < 0) {
		EL((ASSAERR,"Can't GETVAL\n"));
		Assure_exit (false);
	}

	if (semval == 0) {
		/*
		  We could initalize by doing a SETALL, but that
		  would clear the adjust value that we set when
		  we locked the semaphore above. Instead, we'll do
		  two system calls to initialize semaphore value [0]
		  and process counter [1].
		*/
		semctrl_arg.val = initval_;

		if (semctl (m_id, 0, SETVAL, semctrl_arg) < 0) {
			EL((ASSAERR,"Can't SETVAL[0]\n"));
			Assure_exit (false);
		}

		semctrl_arg.val = BIGCOUNT;

		if (semctl (m_id, 1, SETVAL, semctrl_arg) < 0) {
			EL((ASSAERR,"Can't SETVAL[1]\n"));
			Assure_exit (false);
		}
	} // if (semval == 0)

	/*--- Decrement the process counter and then release the lock. ---*/

	if (semop (m_id, &m_op_endcreate[0], 2) < 0) {
		EL((ASSAERR,"Error on semop (ndcreate)\n"));
		Assure_exit (false);
	}
	return (m_id);
}

int
Semaphore::
open (key_t key_)
{
	trace_with_mask("Semaphore::open", SEM);

	if (IPC_PRIVATE == key_) {
		EL((ASSAERR,"Not intended for private semaphores\n"));
		return (-1);
	}
	else if (key_ == (key_t) -1) {
		EL((ASSAERR,"Probably an ftok() error by caller\n"));
		return (-1);
	}

	m_key = key_;

	if ((m_id = semget (m_key, 3, 0)) < 0) {
		EL((ASSAERR,"Error on semget(3)"));
		return (-1);
	}
	/*--- Decrement the process counter. No need for lock ---*/

	if (semop (m_id, &m_op_open[0], 1) < 0) {
		EL((ASSAERR,"Error on semget(open)\n"));
		Assure_exit(false);
	}
	return (m_id);
}

void
Semaphore::
remove ()
{
	trace_with_mask("Semaphore::remove", SEM);

	if (m_id < 0 || m_key == ((key_t) -1) ) return;

	if (semctl (m_id, 0, IPC_RMID, 0) < 0) {
		EL((ASSAERR,"Can't IPC_RMID\n"));
		Assure_exit(false);
	}
	init ();
}

void
Semaphore::
close ()
{
	trace_with_mask("Semaphore::close", SEM);

	register int semval;

	if (m_id < 0) return;

	/*
	  First get the lock on semaphore, then increment process  counter.
	*/
	if (semop (m_id, &m_op_close[0], 3) < 0) {
		EL((ASSAERR,"Can't semop(2)\n"));
		Assure_exit(false);
	}
	/*
	  Now that we have a lock, read the value of the process counter
	  to see if this is the last reference to the semaphore.
	  There is a race condition here (same as in Semaphore::create()).
	*/
	if ((semval = semctl (m_id, 1, GETVAL, 0)) < 0) {
		EL((ASSAERR,"Can't GETVAL\n"));
		Assure_exit(false);
	}

	if (semval > BIGCOUNT) {
		EL((ASSAERR,"sem[1] > BIGCOUNT\n"));
		Assure_exit(false);
	}
	else if (semval == BIGCOUNT) {
		remove ();
	}
	else if (semop (m_id, &m_op_unlock[0], 1) < 0) {
		EL((ASSAERR,"Can't unlock\n"));
		Assure_exit(false);
	}
	/*--- Invalidate ---*/
	init ();
}


void
Semaphore::
op (int value_)
{
    /* Test if m_id is still valid. If it fails, then
	 * next operation is failing because of it. If not,
	 * then something else happens here.
	 */
	trace_with_mask("Semaphore::op", SEM);

	int semval = 0;
	dump ();

	if ((semval = semctl (m_id, 1, GETVAL, 0)) < 0) {
		EL((ASSAERR,"Can't GETVAL\n"));
		Assure_exit (false);
	}

    /* This will fail on Solaris? */

	if ((m_op_op[0].sem_op = value_) == 0) {
		EL((ASSAERR,"Can't have value_ == 0\n"));
		Assure_exit(false);
	}

	if (semop (m_id, &m_op_op[0], 1) < 0) {
		EL((ASSAERR,"sem_op error\n"));
		Assure_exit(false);
	}
}

void
Semaphore::
dump (void) const
{
	trace_with_mask("Semaphore::dump", SEM);

	std::ostringstream msg;
	msg << "\n\n\tKey.....: ";

	if (m_key == (key_t) -1) {
		msg  << m_key;
	}
	else {
		msg << "0x" << std::hex << m_key << std::dec;
	}

	msg << "\n\tID......: " << m_id << "\n\n";

	if (m_id >= 0 && m_key >= (key_t) -1) {
		msg << "\tsemval [0]\tproc counter[1]\tlock [2]\n"
		    << "\t----------\t---------------\t--------\n";

		/*--- Get value of element in semaphore set ---*/
		msg << "\t   "   << semctl (m_id, 0, GETVAL)
		    << "\t\t   " << semctl (m_id, 1, GETVAL)
		    << "\t\t   " << semctl (m_id, 2, GETVAL);
	}
	else {
		msg << "Semaphore id = -1. No info is available.";
	}
	msg << std::ends;
	DL((SEM,"%s\n\n", msg.str ().c_str ()));
}

#endif /* !defined(WIN32) */

