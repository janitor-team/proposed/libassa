// -*- c++ -*-
//------------------------------------------------------------------------------
//                             memdump_test.cpp
//------------------------------------------------------------------------------
// $Id: memdump_test.cpp,v 1.2 2002/12/05 03:20:17 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iostream>
using namespace std;

#include "assa/MemDump.h"
using namespace ASSA;

int main()
{
	std::cout << "= Running memdump_test Test =\n\n";

	char random_16[] = "\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r";
	char random[] = "qwertyuiopasdfghjklzxcvbnmasdfghv";
	char random_60[] = "\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r";
	char random_32[] = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	char data4[] = "Legitimate string ";
	char data5[] = "Legitimate string ";
	long neg_len = -123456789;

	MemDump dump0(random_16, 16);
	cout << "16 control characters:\n" << random_16 << '\n'
		 << dump0.getMemDump() << "\n\n";
	
	MemDump dump1(random, strlen(random));
	cout << "Dump of random-ordered alphabet:\n[" << random << "]\n\n"
		 << dump1.getMemDump() << "\n\n";

	MemDump dump2(random_60, strlen(random_60));
	cout << "60 control characters:\n" << random_60 << '\n'
		 << dump2.getMemDump() << "\n\n";
	
	MemDump dump3(random_32, strlen(random_32));
	cout << "32 control characters:\n\n"
		 << dump3.getMemDump() << "\n\n";

	MemDump dump4(data4, 0);
	cout << data4 << " with 0 data length: \n\n"
		 << dump4.getMemDump() << "\n\n";

	MemDump dump5(data5, neg_len);
	cout << data5 << " with negative " << neg_len << " data length: \n\n"
		 << dump5.getMemDump() << "\n\n";

	std::cout << "Test passed\n";
	return 0;
}


