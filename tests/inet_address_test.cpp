// -*- c++ -*-
//------------------------------------------------------------------------------
//                         inet_address_test.cpp
//------------------------------------------------------------------------------
// $Id: inet_address_test.cpp,v 1.6 2006/07/20 02:30:56 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002,2004 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <vector>
#include <iostream>
using namespace std;

#include <assert.h>

#if !defined (WIN32)
#  include <sys/utsname.h>
#endif

#include "assa/Logger.h"
#include "assa/INETAddress.h"
using namespace ASSA;

typedef std::vector<std::string>::const_iterator AliasesCIter;
std::vector<std::string> g_aliases;

void print_host_and_port (INETAddress& a_)
{
	DL((APP,"\n"
		"\tHost: \"%s\"\n"
		"\tPort: %d\n",
	    a_.getHostName ().c_str (),
		a_.getPort ()));
	a_.dump ();
}

void log_test_header (u_int& index_, const char* message_)
{
	DL ((ALL,"\n=======================================\n"));
	DL((ALL,"TEST %d\n", index_));
	cout << "Test " << index_ << " ...";
	DL((ALL, "%d) %s\n", index_, message_));
	index_++;
}

// Match the hostname_ against aliases list
// Return: true if match exists, false otherwise

bool
match_with_aliases (const std::string& hostname_)
{
	if (g_aliases.size () == 0) {
		return false;
	}
	AliasesCIter citer = g_aliases.begin ();
	while (citer != g_aliases.end ()) {
		if (*citer == hostname_) {
			return true;
		}
		citer++;
	}
	return false;
}

int main (int argc, char* argv[])
{
	char* self = argv[0];
	u_int index = 1;			// test counter

	string logname (self);
	logname += ".log";

	unlink (logname.c_str());
	Log::open_log_file (logname.c_str());

	std::cout << "= Running inet_address_test Test =\n";
	
	/* We are after fully-qualified host name. Kernel (uname(2)) knows
	 * nothing about network and cannot give us our DNS name.
	 * gethostbyname(3), however, can. It is equivalent to calling
	 * dnshostname (1).
	 */
	string hostname (INETAddress::get_fully_qualified_domain_name (g_aliases));

	DL((ALL,"My host FQDN \"%s\"\n", hostname.c_str ()));
	DL((ALL,"My host aliases:\n"));
	if (g_aliases.size () == 0) {
			DL((ALL,"  no aliases\n"));
	}
	else {
		AliasesCIter it = g_aliases.begin ();
		while (it != g_aliases.end ()) {
			DL((ALL," \"%s\"\n", (*it).c_str ()));
			it++;
		}
		DL((ALL," end-of-list\n"));
	}

	const int port = 18;
	int errors = 0;
	// string lochost ("localhost.localdomain");
	string lochost ("localhost");

	/**********************************************************************
     * Test 1
	 */
	log_test_header (index, "create address with default constructor");

	INETAddress addr1;
	cout << " ok\n";

	/**********************************************************************
	 * Test 2
	 */
	log_test_header (index, "create address from (host, port) pair");
	DL((APP," (\"%s\", %d)\n", hostname.c_str (), port));

	INETAddress addr2 (hostname.c_str (), port);
	addr2.dump ();
	DL((APP,"getHostName () = \"%s\"\n", addr2.getHostName ().c_str ()));
	DL((ALL,"getPort () = %d\n", addr2.getPort ()));

	if (addr2.getPort () == port &&
	    (addr2.getHostName () == lochost || 
	     addr2.getHostName () == hostname)) 
	{
		cout << " ok\n";
	}
	else {
		cout << " failed!\n"; errors++;
	}
	print_host_and_port (addr2);

	/**********************************************************************
	 * Test 3
	 */
	log_test_header (index, 
					 "create address from service/tcp ('time','tcp') pair");

	INETAddress addr3 ("time", INETAddress::TCP);

	if (addr3.getPort () != 37) { 
		cout << " failed!\n"; errors++;
	}
	else {
		cout << " ok\n";
	}
	print_host_and_port (addr3);

	/**********************************************************************
	 * Test 4
	 */
	log_test_header (index, "create listen address from service ('time')");

	INETAddress addr4 ("time");

	if (addr4.getPort () != 37) { 
		cout << " failed!\n"; 
		errors++;
	}
	else {
		cout << " ok\n";
	}
	print_host_and_port (addr4);

	/**********************************************************************
	 * Test 5
	 */
	log_test_header (index, 
					 "create address from host:service (\"localhost:time\")");

	INETAddress addr5 ("localhost:time");

	if (addr5.getPort () != 37 || match_with_aliases (addr5.getHostName ())) {
		cout << " failed!\n"; 
		cout << "addr5.getPort() = " << addr5.getPort () << " (expected 37)\n"
			 << "addr5.getHostName () = \"" << addr5.getHostName ()
			 << "\" (expected one from the aliases list)\n";
		errors++;
	}
	else {
		cout << " ok\n";
	}

	print_host_and_port (addr5);

	/**********************************************************************
	 * Test 6
	 */
	log_test_header (index, "create address from service[@hostname]");
	DL((APP," = 'time@%s\n", hostname.c_str ()));
	
	string isiaddr = "time@" + hostname;
	INETAddress addr6 (isiaddr.c_str());

	if (addr6.getPort () == 37 &&
	    (addr6.getHostName () == lochost || addr6.getHostName () == hostname)) 
	{ 
		cout << " ok\n";
	}
	else {
		cout << " failed!\n"; 
		errors++;
	}
	print_host_and_port (addr6);

	/**********************************************************************
	 * Test 7
	 */
	log_test_header (index, "create address from 0@hostname (should fail)");

	string nulladdr = "0@" + hostname;
	INETAddress addr7 (nulladdr.c_str());

	if (!addr7.bad()) { cout << "failed!\n"; errors++; }
	else cout << " ok\n";
	print_host_and_port (addr7);

	/**********************************************************************
	 * Test 8
	 */
	log_test_header (index, "Address 10000@localhost should be ok");

	INETAddress addr8 ("10000@localhost");

	if (addr8) {
		cout << " ok\n";
	}
	else { 
		cout << "failed\n"; 
		errors++; 
	}
	print_host_and_port (addr8);

	/**********************************************************************
	 * Test 9
	 */
	log_test_header (index, "Address ('blahblah@localhost') should fail");

	INETAddress addr9 ("blahblah@localhost");

	if (addr9) { 
		cout << "failed!\n"; errors++; 
	}
	else { 
		cout << " ok\n"; 
	}
	print_host_and_port (addr9);

	/**********************************************************************
	 * Summary
	 */
	if (errors) {
		cout << "Test failed! There were " << errors << " errors\n";
		return 1;
	}
	cout << "Test completed successfully!\n";
	return 0;
}


