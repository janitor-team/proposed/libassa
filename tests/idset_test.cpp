// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: idset_test.cpp,v 1.2 2002/12/05 03:20:17 vlg Exp $
//------------------------------------------------------------------------------
//
// Name: idset_test.C
//
// Synopsis:
//      % idset_test
//
// Description: test program for IdSet class functionality
//-----------------------------------------------------------------------

#include <iostream>
using namespace std;

#include "assa/Logger.h"
#include "assa/IdSet.h"
using namespace ASSA;

int main()
{
	std::cout << "= Running id_set Test =\n";
	int ret = 0;
	int failed = 0;

    cout << endl << "*** Test for id handling:" << endl;

    IdSet ids;

    if (ids.currid() == 0) {
		cout << "Current = 0 ... ok" << endl;
    }
    else {
		cout << "Current = " << ids.currid() << "... error" << endl;
		failed++;
    }

    int i = ids.newid();
    i = ids.newid();

    if (i == 1) {
		cout << "New = 1 ... ok" << endl;
    }
    else {
		cout << "New = " << i << "... error" << endl;
		failed++;
    }

    for (int j=i; j < 100; j++) {
		i = ids.newid();
    }

    i = ids.currid();

    if (i == 101) {
		cout << "Current = 101 ... ok" << endl;
    }
    else {
		cout << "Current = " << i << "... error" << endl;
		failed++;
    }

    cout << endl << "*** Test recycling:" << endl;
    ids.recycle(30);
    ids.recycle(50);
    ids.recycle(75);

    i = ids.currid();

    if (i == 30) {
		cout << "Current = 30 ... ok" << endl;
    }
    else {
		cout << "Current = " << i << "... error" << endl;
		failed++;
    }

    i = ids.newid();
    i = ids.newid();

    if (i == 50) {
		cout << "New = 50 ... ok" << endl;
    }
    else {
		cout << "New = " << i << "... error" << endl;
		failed++;
    }
    cout << endl;

	if (failed) {
		cout << failed << "tests failed\n";
	}
	else {
		cout << "Test passed\n";
	}

    return ret;
}
