// -*- c++ -*-
// Generated by assa-genesis
//------------------------------------------------------------------------------
// $Id: timeval_test.cpp,v 1.6 2005/10/08 02:42:01 vlg Exp $
//------------------------------------------------------------------------------
//                            timeval_test.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2002,2005 by Vladislav Grinchenko
//
//  Permission to use, copy, modify, and distribute this software      
//  and its documentation for any purpose and without fee is hereby    
//  granted, provided that the above copyright notice appear in all    
//  copies.  The author makes no representations about the suitability 
//  of this software for any purpose.  It is provided "as is" without  
//  express or implied warranty.                                       
//------------------------------------------------------------------------------
//
// Date   : Wed Oct 16 15:09:19 2002
//
//------------------------------------------------------------------------------
static const char help_msg[]=
"                                                                            \n"
" NAME:                                                                      \n"
"                                                                            \n"
"   timeval_test                                                             \n"
"                                                                            \n"
" DESCRIPTION:                                                               \n"
"                                                                            \n"
"   Basic test for TimeVal class.                                            \n"
"                                                                            \n"
" USAGE:                                                                     \n"
"                                                                            \n"
"   shell>  timeval_test [OPTIONS]                                           \n"
"                                                                            \n"
" OPTIONS:                                                                   \n"
"                                                                            \n"
" -D, --log-file NAME     - Write debug to NAME file                         \n"
" -d, --log-stdout        - Write debug to standard output                   \n"
" -z, --log-size NUM      - Maximum size debug file can reach (dfl: is 10Mb) \n"
" -m, --mask MASK         - Mask (default: ALL = 0x7fffffff)                 \n"
" -h, --help              - Print this messag                                \n"
" -v, --version           - Print version number                            \n";
//------------------------------------------------------------------------------

#include <string>
using std::string;
#include <iostream>
using namespace std;

#include <assa/GenServer.h>
#include <assa/Singleton.h>
#include <assa/TimeVal.h>
#include <assa/Assure.h>
using namespace ASSA;

class TV_Test :
    public GenServer,
    public Singleton<TV_Test>
{
public:
    TV_Test ();

    virtual void init_service ();
    virtual void process_events ();

private:
	void log_header (const string& msg_);
	void dump_all_formats (TimeVal& tv_, const string& msg_);
};

/* Useful definitions */

#define TV_TEST TV_Test::get_instance()
#define REACTOR TV_TEST->get_reactor()

// Static declarations mandated by Singleton class
ASSA_DECL_SINGLETON(TV_Test);

TV_Test::TV_Test ()
{
    m_log_file = "timeval_test.log";
}

void
TV_Test::init_service ()
{
    trace("TV_Test::init_service");

    Log::disable_timestamp ();

    DL((APP,"Service has been initialized\n"));
}

void
TV_Test::
log_header (const string& msg_)
{
	cout << msg_;
	DL((APP,"%s\n", msg_.c_str ()));
}

void
TV_Test::
dump_all_formats (TimeVal& tv_, const string& msg_)
{
	DL((APP, "\n"
		"\t%s\n"
		"\t===========================================\n"
		"\t sec = %d\n"
		"\t msec = %d\n"
		"\t millisec = %d\n"
		"\t fmtString()         = %s\n"
		"\t fmt_hh_mm_ss()      = %s\n",
		msg_.c_str (),
		tv_.sec (), 
		tv_.msec (), 
		tv_.millisec (),
		tv_.fmtString        ().c_str (),
		tv_.fmt_hh_mm_ss     ().c_str ()));

	DL((APP,"\n"
		"\t fmt_hh_mm_ss_mls()  = %s\n"
		"\t fmt_mm_ss()         = %s\n"
		"\t fmt_mm_ss_mls()     = %s\n"
		"\t fmt_ss_mls()        = %s\n"
		"\t===========================================\n",
		tv_.fmt_hh_mm_ss_mls ().c_str (),
		tv_.fmt_mm_ss        ().c_str (),
		tv_.fmt_mm_ss_mls    ().c_str (),
		tv_.fmt_ss_mls       ().c_str ()
		   ));
}

void
TV_Test::process_events ()
{
    trace("TV_Test::process_events");

	log_header ("1) Testing constructors ...");
	double result;

	TimeVal tv0(TimeVal::gettimeofday ());		// today's date
	DL((APP,"Time now: %s -> %5.2f\n", tv0.fmtString().c_str (), double(tv0)));

	TimeVal tv1(1, 0);	// 1 second
	DL((APP,"1 sec   : %s -> %5.2f\n", tv1.fmtString().c_str (), double(tv1)));
	result = tv1;
	Assure_exit (result == 1.0);

	TimeVal tv2(2.5);	// 2 1/2 secs
	DL((APP,"2.5 secs: %s -> %5.2f\n", tv2.fmtString().c_str (), double(tv2)));
	result = tv2;
	Assure_exit (result == 2.5);

	TimeVal tv_05 (.5);	// 1/2 of a second
	DL((APP,"0.5 secs: %s -> %5.2f\n", 
		tv_05.fmtString().c_str (), double (tv_05)));
	result = tv_05;
	Assure_exit (result == 0.5);

	cout << "ok\n";

	log_header ("2) Testing conversion to/from struct timeval ...");

	time_t  tt;
	time (&tt);
	struct timeval tval = { tt, 0 };
	TimeVal tv3(tval);     

	TimeVal tv4(tv3);
	tval = tv4;
	cout << "ok\n";

	log_header ("3) Testing assignment opt ...");

	TimeVal tv5;
	tv5 = tv4;
	Assure_exit (tv5 == tv4);

	cout << "ok\n";

	log_header ("4) Testing +/- opts ...");

	tv5 += tv1;
	tv5 -= tv1;
	Assure_exit (tv4 == tv5);

	cout << "ok\n";
	
	log_header ("5) Testing logical opts ...");

	Assure_exit (tv1 < tv2);
	Assure_exit (tv2 > tv1);
	Assure_exit (tv1 <= tv2);
	Assure_exit (tv2 >= tv1);
	Assure_exit (tv1 != tv2);

	cout << "ok\n";

	log_header ("6) Testing default formatting ...");

	DL((APP, "GMT: %s\n", tv0.fmtString ().c_str ()));
	tv0.tz (TimeVal::loc);
	DL((APP, "EDT: %s\n", tv0.fmtString ().c_str ()));

	cout << "ok\n";

	log_header ("7) Testing user-specified formatting ...");

	tv0.tz (TimeVal::gmt);
	DL((APP,"GMT: %s\n", tv0.fmtString ("%c").c_str ()));

	tv0.tz (TimeVal::loc);
	DL((APP,"EDT: %s\n", tv0.fmtString ().c_str ()));

	cout << "ok\n";

	log_header ("8) Testing formatting functions ...");

	TimeVal tv81 (27.348);
	dump_all_formats (tv81, "Formatting for 27 seconds 348 milliseconds:");
	tv81.dump_to_log ("tv81");

	TimeVal tv82 (30);
	dump_all_formats (tv82, "Formatting for 30 secs");
	tv82.dump_to_log ("tv82");

	TimeVal tv83 (0);
	dump_all_formats (tv83, "Formatting for 0 seconds");
	tv83.dump_to_log ("tv83");
	
	cout << "ok\n";

	log_header ("9) Testing default initialization ...");

	if (tv83 == TimeVal::zeroTime ()) { 
		DL((APP,"tv83 is equal to 0 - OK\n"));
		cout << "ok\n";
	}
	else {
		DL((APP,"Oops! tv83 is not equal to 0\n - ERROR\n"));
		cout << "error\n";
		set_exit_value (1);
	}

    m_reactor.stopReactor ();
    DL((APP,"Service stopped!\n"));
}

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

int
main (int argc, char* argv[])
{
    static const char release[] = "VERSION";
    int patch_level = 0;

	cout << "= Running timeval_test Test =\n";

    TV_TEST->set_version (release, patch_level);
    TV_TEST->set_author  ("Vladislav Grinchenko");
	TV_TEST->set_flags (GenServer::RMLOG);

    TV_TEST->init (&argc, argv, help_msg);
 
    TV_TEST->init_service ();
    TV_TEST->process_events ();

	cout << "Test " 
		 << (TV_TEST->get_exit_value () == 0 ? "passed\n" : "failed\n");

    return TV_TEST->get_exit_value ();
}

